from django.conf.urls import url, include
from django.contrib.auth.views import login
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'profile.html'

    
urlpatterns = [
    url(r'^login/$', login, {'template_name': 'admin/login.html'}),
    url(r'^profile/$', ProfileView.as_view()),
    url('^', include('django.contrib.auth.urls')),
]
