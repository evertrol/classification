from django.conf.urls import include, url


urlpatterns = [
    url(r'^vetting/',
        include('sourcevetting.urls', namespace='sourcevetting')),

    #url(r'^obslog/',
    #            include('obslog.urls', namespace='obslog')),
    
    url(r'^',
        include('classification.apps.main.urls', namespace='main')),
]
