from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from markdownx import urls as markdownx


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^markdownx/', include(markdownx)),
    url(r'^accounts/', include('classification.account_urls')),
    url(r'^', include('classification.apps.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
